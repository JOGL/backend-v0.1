# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/saveable'

RSpec.describe Api::NeedsController, type: :controller do
  context 'Examples' do
    describe 'need relations' do
      it_behaves_like 'an object with followable', :need
      it_behaves_like 'an object with saveable', :need
    end

    describe 'Need' do
      it_behaves_like 'an object with members', :need
      it_behaves_like 'an object with media', :need
      it_behaves_like 'an object with recommendations', :need
    end
  end

  context 'Needs specs' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @project = create(:project, creator_id: @user.id)
      @need = create(:need, project_id: @project.id, user_id: @user.id)
    end

    describe '#index' do
      it 'should return list of need' do
        get :index
        expect(response).to have_http_status :ok
      end
    end

    describe '#my_needs' do
      it 'should return list of the needs of current user' do
        get :my_needs
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response.size).to eq(1)
      end
    end

    describe '#create' do
      context 'sad path' do
        it 'declines to create a need when project_id is missing' do
          expect do
            post :create, params: { need: { project_id: nil } }
          end.to change(Need, :count).by(0)
          expect(response.status).to eq(404)
          json_response = JSON.parse(response.body)
          expect(json_response['data']).to eq('Project not found. Did you specify a project_id?')
        end

        it 'declines to create a need when project_id is not for a valid project' do
          need_attrs = build(:need).attributes.merge!('project_id' => 999)
          expect do
            post :create, params: { need: need_attrs }
          end.to change(Need, :count).by(0)
          expect(response.status).to eq(404)
          json_response = JSON.parse(response.body)
          expect(json_response['data']).to match(/Project with id \d+ not found./)
        end

        it 'declines to create a need when project exists but user is not member of project' do
          other_users_project = create(:project)
          need_attrs = build(:need).attributes.merge!('project_id' => other_users_project.id)
          expect do
            post :create, params: { need: need_attrs }
          end.to change(Need, :count).by(0)
          expect(response.status).to eq(403)
          json_response = JSON.parse(response.body)
          expect(json_response['data']).to match(/You must be a member of the project id: \d+ to create a need/)
        end
      end

      context 'creating need with logged in user and is member of project' do
        it 'creates a need' do
          @user.add_role :member, @project
          post :create, params: { need: @need.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          need = Need.find(json_response['id'])
          expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: need.class.name, targetable_node_id: need.id, relation_type: 'is_author_of')).to exist
          expect(RecsysDatum.where(sourceable_node_type: 'Project', sourceable_node_id: need.project.id, targetable_node_type: need.class.name, targetable_node_id: need.id, relation_type: 'has_need')).to exist
        end
      end

      context 'creating need without logged in user' do
        it 'returns unauthorized error' do
          @user.add_role :member, @project
          sign_out @user
          post :create, params: { need: @need.attributes }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#show' do
      it 'should return need' do
        get :show, params: { id: @need.id }
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @need.class.name, targetable_node_id: @need.id, relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      # context 'Updating need with logged in user with owner role of need' do
      #   it 'Updates a need' do
      #     @user.add_role :owner, @need
      #     put :update, params: { need: { content: 'New content', skills: ['Big Data'], ressources: ['3D printers'], end_date: '20160911' }, id: @need.id }
      #     json_response = JSON.parse(response.body)
      #     expect(response.status).to eq 200
      #     expect(json_response['data']).to eq "Need id:#{@need.id} has been updated"
      #     @need.reload
      #     expect(@need.skills.count).to eq 1
      #     expect(@need.ressources.count).to eq 1
      #     expect(@need.ressources.first.ressource_name).to match('3d printer')
      #     expect(@need.end_date.to_s).to match('2016-09-11 00:00:00')
      #   end
      # end

      context 'Updating need without logged in user' do
        it 'should not update the need' do
          sign_out @user
          put :update, params: { need: { content: 'New content' }, id: @need.id }
          expect(response.status).to eq 401
        end
      end

      context 'User not authorized for updating need' do
        before(:each) do
          @need = create(:need)
        end

        it 'Should throw forbidden error' do
          put :update, params: { need: { content: 'New content' }, id: @need.id }
          expect(response.status).to eq 403
        end
      end
    end

    describe '#destroy' do
      context 'Deleting need with user not having owner role of need' do
        before(:each) do
          @need = create(:need)
        end

        it 'Should not delete the need' do
          delete :destroy, params: { id: @need.id }
          expect(response.status).to eq 403
        end
      end

      context 'deleting need without logged in user' do
        it 'Should not Delete the need' do
          sign_out @user
          delete :destroy, params: { id: @need.id }
          expect(response.status).to eq 401
        end
      end

      context 'deleting need with logged in user with owner role' do
        it 'Should Delete the need' do
          @user.add_role :owner, @need
          delete :destroy, params: { id: @need.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq "Your need id:#{@need.id} has been deleted"
        end
      end
    end
  end
end
