# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/affiliation'
require_relative 'shared_examples/faqable'
require_relative 'shared_examples/followable'
require_relative 'shared_examples/linkable'
require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/memberable'
require_relative 'shared_examples/recommendable'
require_relative 'shared_examples/resourceable'
require_relative 'shared_examples/saveable'

RSpec.describe Api::ProgramsController, type: :controller do
  context 'Shared Examples' do
    describe 'program relations' do
      it_behaves_like 'an object with followable', :program
      it_behaves_like 'an object with saveable', :program
    end

    describe 'Program' do
      it_behaves_like 'an object with affiliations', :program
      it_behaves_like 'an object with an faq', :program
      it_behaves_like 'an object with links', :program
      it_behaves_like 'an object with media', :program
      it_behaves_like 'an object with members', :program
      it_behaves_like 'an object with recommendations', :program
      it_behaves_like 'an object with resources', :program
    end
  end

  describe 'object with sub object members' do
    describe '#members_list' do
      before do
        @megan = create(:confirmed_user, first_name: 'Megan', last_name: 'Strant')
        sign_in @megan

        @program = create(:program) # creates user 2
        @program.admins.first.update(first_name: 'Bill')
        @megan.add_role :admin, @program
        @challenge = create(:challenge, program: @program) # creates user 3
        @challenge.admins.first.update(first_name: 'Jean')
        damaris = create(:confirmed_user, first_name: 'Damaris')
        damaris.add_role :member, @challenge
      end

      it 'should get the owners/admins/members of the project and members of sub objects' do
        project = create(:project, creator: create(:confirmed_user, first_name: 'Calvin')) # note: adds creator as a member
        @challenge.projects << project
        roberta = create(:confirmed_user, first_name: 'Roberta')
        roberta.add_role(:member, project)
        michael = create(:confirmed_user, first_name: 'Michael')
        michael.add_role(:pending, project)
        owner = create(:confirmed_user, first_name: 'Owner')
        owner.add_role(:owner, project)
        admin = create(:confirmed_user, first_name: 'Admin')
        admin.add_role(:admin, project)

        get :members_list, params: { id: @program.id }

        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        # assert that users with only admin/owner roles on sub objects do
        # show up in members list, and have 'member' set to true
        expect(json_response['members'].count).to eq(8)
        expect(json_response['members'].map { |m| m['first_name'] }).to eq(%w[Megan Bill Jean Damaris Calvin Roberta Owner Admin])
        expect(json_response['members'].all? { |m| m['member'] }).to eq(true)
      end

      xit 'should order the returned users correctly' do
        get :members_list, params: { id: @program.id }
        json_response = JSON.parse(response.body)['members']
        expect(json_response.count).to eq(3)

        expect(json_response[0]['owner']).to eq(true)
        expect(json_response[1]['admin']).to eq(true)
        expect(json_response[2]['member']).to eq(true)
      end
    end
  end

  before do
    @program = create(:program)
  end

  context 'Controller specs' do
    before do
      @user = @program.users.first
      sign_in @user
    end

    describe '#index' do
      it 'should return list of program' do
        @program.status = :active
        @program.save
        get :index
        expect(response).to have_http_status :ok
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq 1
      end
    end

    describe '#my_programs' do
      context 'getting users programs with logged in user' do
        it 'should return list of users programs' do
          get :my_programs
          expect(response).to have_http_status :ok
          json_response = JSON.parse(response.body)
          expect(json_response.count).to eq 1
        end
      end

      context 'getting users programs without logged in user' do
        it 'should return list of users programs' do
          sign_out @user
          get :my_programs
          expect(response).to have_http_status :unauthorized
        end
      end
    end

    describe '#create' do
      context 'without logged in user' do
        it "Return Unauthorized because user's not signed in" do
          sign_out @user
          post :create, params: { program: @program.attributes }
          expect(response.status).to eq 401
        end
      end

      context 'creating program with unapproved user' do
        it 'returns forbidden' do
          @program.short_title = FFaker::Name.first_name
          post :create, params: { program: @program.attributes }
          expect(response.status).to eq 403
        end
      end

      context 'creating program with approved user' do
        it 'creates a program' do
          program = build(:program)
          @user.add_role :program_creator
          post :create, params: { program: program.attributes }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 201
          expect(json_response['data']).to eq 'Success'
        end
      end
    end

    describe '#show' do
      it 'should return program' do
        get :show, params: { id: @program.id }
        json_response = JSON.parse(response.body)
        expect(response.status).to eq 200
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: 'Program', targetable_node_id: json_response['id'], relation_type: 'has_visited')).to exist
      end
    end

    describe '#update' do
      context 'Updating program' do
        it 'Updates a program' do
          @user.add_role :admin, @program
          put :update, params: { program: { description: 'New description' }, id: @program.id }
          expect(response.status).to eq 200
        end

        it "from 'draft' to 'soon' status which triggers notifications" do
          @user.add_role :member, @program
          @user.add_role :admin, @program
          expect do
            put :update, params: { program: { status: 'soon' }, id: @program.id }
          end #.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          # notif = Notification.where(object_type: @program.class.name, object_id: @program.id, type: 'soon_program')
          # expect(notif.count).to eq(User.count)
          # expect(notif.first.category).to match('program')
        end

        it "from 'soon' to 'active' status which triggers notifications" do
          @program.soon!
          @user.add_role :member, @program
          @user.add_role :admin, @program
          expect do
            put :update, params: { program: { status: 'active' }, id: @program.id }
          end#.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          # notif = Notification.where(object_type: @program.class.name, object_id: @program.id, type: 'start_program')
          # expect(notif.count).to eq(User.count)
          # expect(notif.first.category).to match('program')
        end

        it "from 'active' to 'completed' status which triggers notifications" do
          @program.active!
          @user.add_role :member, @program
          @user.add_role :admin, @program
          expect do
            put :update, params: { program: { status: 'completed' }, id: @program.id }
          end#.to change(Notification, :count).by(User.all.count)
          expect(response.status).to eq 200
          # notif = Notification.where(object_type: @program.class.name, object_id: @program.id, type: 'end_program')
          # expect(notif.count).to eq(User.count)
          # expect(notif.first.category).to match('program')
        end
      end

      context 'User not logged in' do
        it "Should throw Unauthorized error and won't update the program" do
          sign_out @user
          put :update, params: { program: { description: 'New description' }, id: @program.id }
          expect(response.status).to eq 401
        end
      end
    end

    describe '#destroy' do
      context 'User not logged in' do
        it 'Should Not Delete the program' do
          sign_out @user
          delete :destroy, params: { id: @program.id }
          expect(response.status).to eq 401
        end
      end

      context 'User not logged in' do
        it 'Should Not Delete the program' do
          delete :destroy, params: { id: @program.id }
          expect(response.status).to eq 200
        end
      end
    end

    describe '#attach' do
      context 'attaching a challenge to program with logged in user' do
        it 'should attach a challenge to a program' do
          challenge = create(:challenge)
          put :attach, params: { id: @program.id, challenge_id: challenge.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq "Challenge id:#{challenge.id} attached"
        end
      end
    end

    describe '#get_challenges' do
      context 'we can get the list of challenges attached to a program' do
        it 'should return the list of challenges' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          @program.challenges << challenge1 << challenge2
          get :index_challenges, params: { id: @program.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['challenges'].count).to eq 2
        end
      end
    end

    describe '#index_projects' do
      context 'we can get the list of projects attached to a program' do
        it 'should return the list of projects' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          project1 = create(:project, creator_id: @user.id)
          project2 = create(:project, creator_id: @user.id)
          project3 = create(:project, creator_id: @user.id)

          challenge1.projects << project1 << project2
          @program.challenges << challenge1 << challenge2
          challenge2.projects << project3 << project1
          get :index_projects, params: { id: @program.id }
          expect(response.status).to eq 200
          json_response = JSON.parse(response.body)
          expect(json_response['projects'].count).to eq 3
          (0..2).each do |i|
            expect(json_response['projects'][i]['challenges'].count).to be >= 0
          end
        end
      end
    end

    describe '#index_needs' do
      context 'we can get the list of needs attached to a program' do
        it 'should return the list of needs' do
          challenge1 = create(:challenge)
          challenge2 = create(:challenge)
          project1 = create(:project, creator_id: @user.id)
          project2 = create(:project, creator_id: @user.id)
          project3 = create(:project, creator_id: @user.id)
          project1.needs << create(:need, project_id: project1.id, user_id: @user.id)
          project1.needs << create(:need, project_id: project1.id, user_id: @user.id)
          project2.needs << create(:need, project_id: project2.id, user_id: @user.id)
          project3.needs << create(:need, project_id: project3.id, user_id: @user.id)
          challenge1.projects << project1 << project2
          challenge1.challenges_projects.update_all(project_status: :accepted)
          @program.challenges << challenge1 << challenge2
          challenge2.projects << project1 << project3
          challenge2.challenges_projects.update_all(project_status: :accepted)
          get :index_needs, params: { id: @program.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['needs'].count).to eq 4
        end
      end
    end

    describe '#remove' do
      context 'removing a challenge from program with logged in user' do
        it 'should remove a challenge from a program' do
          challenge = create(:challenge)
          @program.challenges << challenge
          delete :remove, params: { id: @program.id, challenge_id: challenge.id }
          json_response = JSON.parse(response.body)
          expect(response.status).to eq 200
          expect(json_response['data']).to eq "Challenge id:#{challenge.id} removed"
        end
      end
    end

    describe 'Banner Upload' do
      it 'should allow a PNG image' do
        @user.add_role :admin, @program
        png = fixture_file_upload('/authorized.png', 'image/png')
        post :upload_banner, params: { id: @program.id, banner: png }
        expect(response).to be_successful
      end

      it 'should not allow a TIFF image' do
        @user.add_role :admin, @program
        tiff = fixture_file_upload('/unauthorized.tiff', 'image/tiff')
        post :upload_banner, params: { id: @program.id, banner: tiff }
        expect(response.status).to eq(422)
      end
    end
  end
end
