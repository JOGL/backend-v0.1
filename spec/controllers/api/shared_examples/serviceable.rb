# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'with serviceable' do |factory|
  let(:obj) { create(factory) }

  describe '#index_activity' do
    it 'returns activities for the object' do
      service = create(:service, serviceable_id: obj.id)
      create(:activity, id: [service.id, FFaker::Guid.guid])

      get :index_activity, params: { id: obj.id }
      json_response = JSON.parse(response.body)

      expect(json_response[0]['service_id']).to eq(service.id)
    end
  end

  describe '#index_service' do
    it 'returns services for the object' do
      user = create(:confirmed_user)
      user.add_role(:admin, obj)
      create(:service, serviceable_id: obj.id, creator: user)

      sign_in user

      get :index_service, params: { id: obj.id }
      json_response = JSON.parse(response.body)

      expect(json_response[0]['serviceable_id']).to eq(obj.id)
    end
  end
end
