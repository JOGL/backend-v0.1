# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with an faq' do |factory|
  before do
    @object = create(factory)
    @object.create_faq
  end

  let(:entry) { build(:document).attributes }

  context 'is admin' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      @user.add_role :admin, @object
    end

    describe 'GET #faq_index' do
      before do
        @docs = create_list(:document, 2)
        @docs.each do |doc|
          @object.faq.documents << doc
        end
      end

      it 'returns the FAQ entries' do
        get :faq_index, params: { id: @object.id }
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        expect(json_response['documents'].count).to eq 2
        expect(json_response['documents'].pluck('id')).to include(*@docs.pluck(:id))
      end
    end

    describe 'GET #faq_show' do
      before do
        @docs = create_list(:document, 2)
        @docs.each do |doc|
          @object.faq.documents << doc
        end
        @document = @docs.last
      end

      it 'returns the single entry' do
        get :faq_show, params: { id: @object.id, document_id: @document.id }
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        expect(json_response['title']).to eq @document.title
        expect(json_response['content']).to eq @document.content
      end
    end

    describe 'POST #faq_create' do
      it 'can create an entry' do
        expect do
          post :faq_create, params: { id: @object.id, faq: entry }
        end.to change(@object.faq.documents, :count).by(1)
        expect(response).to have_http_status(:created)
      end

      it 'cannot create with wrong attributes' do
        expect do
          post :faq_create, params: { id: @object.id, faq: { title: 'no content' } }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    describe 'PUT #faq_update' do
      before do
        @document = create(:document)
        @object.faq.documents << @document
      end

      it 'can update an entry' do
        put :faq_update, params: { id: @object.id, document_id: @document.id, faq: entry }
        expect(response).to have_http_status(:ok)
        @document.reload
        expect(@document.title).to eq entry['title']
        expect(@document.content).to eq entry['content']
      end

      it 'cannot update with wrong attributes' do
        post :faq_create, params: { id: @object.id, document_id: @document.id, faq: { title: '' } }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    describe 'DELETE #faq_destroy' do
      before do
        @document = create(:document)
        @object.faq.documents << @document
      end

      it 'can destroy an entry' do
        expect do
          delete :faq_destroy, params: { id: @object.id, document_id: @document.id }
        end.to change(@object.faq.documents, :count).by(-1)
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        @object.reload
        expect(json_response['documents'].count).to eq @object.faq.documents.count
      end
    end
  end

  context 'is NOT admin' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe 'GET #faq_index' do
      before do
        @docs = create_list(:document, 2)
        @docs.each do |doc|
          @object.faq.documents << doc
        end
      end

      it 'returns the FAQ entries' do
        get :faq_index, params: { id: @object.id }
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        expect(json_response['documents'].count).to eq 2
        expect(json_response['documents'].pluck('id')).to include(*@docs.pluck(:id))
      end
    end

    describe 'GET #faq_show' do
      before do
        @docs = create_list(:document, 2)
        @docs.each do |doc|
          @object.faq.documents << doc
        end
        @document = @docs.last
      end

      it 'returns the single entry' do
        get :faq_show, params: { id: @object.id, document_id: @document.id }
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        expect(json_response['title']).to eq @document.title
        expect(json_response['content']).to eq @document.content
      end
    end

    describe 'POST #faq_create' do
      it 'cannot create an entry' do
        expect do
          post :faq_create, params: { id: @object.id, faq: entry }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:forbidden)
      end

      it 'cannot create with wrong attributes' do
        expect do
          post :faq_create, params: { id: @object.id, faq: {} }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe 'PUT #faq_update' do
      before do
        @document = create(:document)
        @object.faq.documents << @document
      end

      it 'can update an entry' do
        put :faq_update, params: { id: @object.id, document_id: @document.id, faq: entry }
        expect(response).to have_http_status(:forbidden)
        expect(@document.title).not_to eq entry['title']
        expect(@document.content).not_to eq entry['content']
      end

      it 'cannot update with wrong attributes' do
        post :faq_create, params: { id: @object.id, document_id: @document.id, faq: {} }
        expect(response).to have_http_status(:forbidden)
      end
    end

    describe 'DELETE #faq_destroy' do
      before do
        @document = create(:document)
        @object.faq.documents << @document
      end

      it 'can destroy an entry' do
        expect do
          delete :faq_destroy, params: { id: @object.id, document_id: @document.id }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:forbidden)
      end
    end
  end

  context 'NOT Signed in' do
    describe 'GET #faq_index' do
      before do
        @docs = create_list(:document, 2)
        @docs.each do |doc|
          @object.faq.documents << doc
        end
      end

      it 'returns the FAQ entries' do
        get :faq_index, params: { id: @object.id }
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        expect(json_response['documents'].count).to eq 2
        expect(json_response['documents'].pluck('id')).to include(*@docs.pluck(:id))
      end
    end

    describe 'GET #faq_show' do
      before do
        @docs = create_list(:document, 2)
        @docs.each do |doc|
          @object.faq.documents << doc
        end
        @document = @docs.last
      end

      it 'returns the single entry' do
        get :faq_show, params: { id: @object.id, document_id: @document.id }
        expect(response).to have_http_status(:ok)
        json_response = JSON.parse(response.body)
        expect(json_response['title']).to eq @document.title
        expect(json_response['content']).to eq @document.content
      end
    end

    describe 'POST #faq_create' do
      it 'cannot create an entry' do
        expect do
          post :faq_create, params: { id: @object.id, faq: entry }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:unauthorized)
      end

      it 'cannot create with wrong attributes' do
        expect do
          post :faq_create, params: { id: @object.id, faq: {} }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:unauthorized)
      end
    end

    describe 'PUT #faq_update' do
      before do
        @document = create(:document)
        @object.faq.documents << @document
      end

      it 'can update an entry' do
        put :faq_update, params: { id: @object.id, document_id: @document.id, faq: entry }
        expect(response).to have_http_status(:unauthorized)
        expect(@document.title).not_to eq entry['title']
        expect(@document.content).not_to eq entry['content']
      end

      it 'cannot update with wrong attributes' do
        post :faq_create, params: { id: @object.id, document_id: @document.id, faq: {} }
        expect(response).to have_http_status(:unauthorized)
      end
    end

    describe 'DELETE #faq_destroy' do
      before do
        @document = create(:document)
        @object.faq.documents << @document
      end

      it 'can destroy an entry' do
        expect do
          delete :faq_destroy, params: { id: @object.id, document_id: @document.id }
        end.to change(@object.faq.documents, :count).by(0)
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
