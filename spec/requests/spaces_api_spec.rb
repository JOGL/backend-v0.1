require 'rails_helper'
require_relative 'api_login'

RSpec.describe 'Spaces', type: :request do
  let(:bob) { create(:confirmed_user, first_name: 'Bob', email: 'bob@example.com') }

  before do
    api_login(bob)
    @auth_params = get_auth_params_from_login_response_headers(response)
  end

  describe "getting affiliates" do

    # To uncommented when this part of affiliates method will be uncomment too.

    # it "returns all affiliates if no type specified" do
    #   space = create(:space)
    #   project = create(:project)
    #   space.add_affiliate(project)
    #   program = create(:program)
    #   space.add_affiliate(program)
    #   get affiliates_api_space_path(space)

    #   json_response = JSON.parse(response.body)
    #   expect(json_response['projects'].size).to eq(1)
    #   expect(json_response['projects'][0]["title"]).to eq(project.title)
    #   expect(json_response['programs'].size).to eq(1)
    #   expect(json_response['programs'][0]["title"]).to eq(program.title)
    # end
    # 
    # it "returns a list of affiliates of that type" do
    #   space = create(:space)
    #   project_0 = create(:project)
    #   space.add_affiliate(project_0)
    #   project_1 = create(:project)
    #   space.add_affiliate(project_1)
    #   program = create(:program)
    #   space.add_affiliate(program)
    #
    #   get affiliates_api_space_path(space), params: { affiliate_type: "Project"}
    #
    #   json_response = JSON.parse(response.body)
    #   expect(json_response['projects'].size).to eq(2)
    #   expect(json_response['projects'][0]["title"]).to eq(project_0.title)
    #   expect(json_response['projects'][1]["title"]).to eq(project_1.title)
    #   expect(json_response['programs']).to be_nil
    # end
  end
end
