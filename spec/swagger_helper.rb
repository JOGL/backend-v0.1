# frozen_string_literal: true

require 'rails_helper'
RSpec.configure do |config|
  # Specify a root folder where Swagger JSON files are generated
  # NOTE: If you're using the rswag-api to serve API descriptions, you'll need
  # to ensure that it's configured to serve Swagger from the same folder
  config.swagger_root = "#{Rails.root}/swagger"

  # Define one or more Swagger documents and provide global metadata for each one
  # When you run the 'rswag:specs:to_swagger' rake task, the complete Swagger will
  # be generated at the provided relative path under swagger_root
  # By default, the operations defined in spec files are added to the first
  # document below. You can override this behavior by adding a swagger_doc tag to the
  # the root example_group in your specs, e.g. describe '...', swagger_doc: 'v2/swagger.json'
  config.swagger_docs = {
    'v1/swagger.json' => {
      swagger: '2.0',
      info: {
        title: 'API V1',
        version: 'v1'
      },
      definitions: {
        createUser: {
          type: :object,
          properties: {
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  required: %i[email password password_confirm],
                  properties: {
                    email: { type: :string, example: 'test@user.com' },
                    password: { type: :string, example: 'password' },
                    password_confirmation: { type: :string, example: 'password' }
                  }
                }
              }
            }
          }
        },
        createProject: {
          type: :object,
          properties: {
            data: {
              type: :object,
              properties: {
                project: {
                  type: :object,
                  required: %i[creator_id title short_title description short_description is_private],
                  properties: {
                    creator_id: { type: :integer, example: 1 },
                    status: { type: :string, example: 'draft' },
                    title: { type: :string, example: 'A nice title' },
                    short_title: { type: :string, example: 'ShortTitle' },
                    logo_url: { type: :string, example: 'https://my.domain/image.jpg' },
                    description: { type: :string, example: 'A long description' },
                    short_description: { type: :string, example: 'A short description' },
                    is_private: { type: :bool, example: true },
                    interests: { type: :array, items: { type: :integer }, example: '[1, 2]' },
                    skills: { type: :array, items: { type: :string }, example: '["Python", "3D printing"]' }
                  }
                }
              }
            }
          }
        },
        createWorkgroup: {
          type: :object,
          properties: {
            data: {
              type: :object,
              properties: {
                workgroup: {
                  type: :object,
                  required: %i[creator_id title short_title description short_description is_private],
                  properties: {
                    creator_id: { type: :integer, example: 1 },
                    status: { type: :string, example: 'draft' },
                    title: { type: :string, example: 'A nice title' },
                    short_title: { type: :string, example: 'ShortTitle' },
                    logo_url: { type: :string, example: 'https://my.domain/image.jpg' },
                    description: { type: :string, example: 'A long description' },
                    short_description: { type: :string, example: 'A short description' },
                    is_private: { type: :bool, example: true },
                    interests: { type: :array, items: { type: :integer }, example: '[1, 2]' },
                    skills: { type: :array, items: { type: :string }, example: '["Python", "3D printing"]' }
                  }
                }
              }
            }
          }
        },
        updateUser: {
          type: :object,
          properties: {
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  required: %i[id],
                  properties: {
                    id: { type: :integer, example: '1' },
                    email: { type: :string, example: 'test@user.com' },
                    first_name: { type: :string, example: 'John' },
                    last_name: { type: :string, example: 'Doe' },
                    nickname: { type: :string, example: 'johndoe' },
                    age: { type: :integer, example: '24' },
                    country: { type: :string, example: 'France' },
                    city: { type: :string, example: 'Paris' },
                    address: { type: :string, example: '15 rue de la science' },
                    phone_number: { type: :string, example: '+33654637383' },
                    bio: { type: :string, example: "This is my bio, it's great!" },
                    interests: { type: :array, items: { type: :integer }, example: '[1, 2]' },
                    skills: { type: :array, items: { type: :string }, example: '["Python", "3D printing"]' }
                  }
                }
              }
            }
          }
        },
        joinProject: {
          type: :object,
          properties: {
            data: {
              type: :object,
              required: %i[user_id],
              properties: {
                user_id: { type: :integer, example: 1 }
              }
            }
          }
        },
        followUser: {
          type: :object,
          properties: {
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  required: %i[other_user_id],
                  properties: {
                    other_user_id: { type: :integer, example: '1' }
                  }
                }
              }
            }
          }
        },
        unfollowUser: {
          type: :object,
          properties: {
            data: {
              type: :object,
              properties: {
                user: {
                  type: :object,
                  required: %i[other_user_id],
                  properties: {
                    other_user_id: { type: :integer, example: '1' }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
end
