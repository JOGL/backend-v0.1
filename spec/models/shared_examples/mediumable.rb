# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'a model that has many media' do |factory|
  let(:object) { build(factory) }

  describe 'has many media' do
    it { should have_many(:media) }

    it 'can recover associated media' do
      create(:medium, mediumable: object)
      create(:medium, mediumable: object)
      expect(object.media.count).to eq 2
    end
  end
end
