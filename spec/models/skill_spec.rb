# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Skill, type: :model do
  it { should have_and_belong_to_many(:challenge) }
  it { should have_and_belong_to_many(:workgroup) }
  it { should have_and_belong_to_many(:needs) }
  it { should have_and_belong_to_many(:projects) }
  it { should have_and_belong_to_many(:users) }
end
