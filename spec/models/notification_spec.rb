# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notification, type: :model do
  describe 'subject_line' do
    describe 'notification types' do
      it 'new_need' do
        create(:need, user: create(:user, first_name: 'George', last_name: 'Pinky'), project: create(:project, title: 'Banana Stand'))
        notification = Notification.last
      end
    end

    it 'new_comment' do
      george = create(:user, first_name: 'George')
      post = create(:post, user: george)
      # create(:comment, post: post, user: george)
      fred = create(:user, first_name: 'Fred', last_name: 'Wonka')

      # it should notify anyone participating in the post who is not the current commenter
      expect do
        create(:comment, post: post, user: fred)
      end.to change { Notification.count }.by(1)

      notification = Notification.last
      expect(notification.subject_line).to eq('Fred Wonka commented on a post you participated in')
    end

    it 'new_comment by post author' do
      george = create(:user, first_name: 'George', last_name: 'Pinky')
      post = create(:post, user: george)
      # create(:comment, post: post, user: george)
      fred = create(:user, first_name: 'Fred', last_name: 'Wonka')
      create(:comment, post: post, user: fred)
      tammy = create(:user, first_name: 'Tammy', last_name: 'Poridge')
      create(:comment, post: post, user: tammy)

      # it should notify anyone participating in the post who is not the current commenter
      expect do
        create(:comment, post: post, user: george)
      end.to change { Notification.count }.by(2)

      notifications = Notification.last(2)
      expect(notifications.first.subject_line).to eq('George Pinky commented on a post you participated in')
      expect(notifications.map(&:target)).to include(fred, tammy)
    end

    it 'new_clap' do
      post = create(:post, user: create(:user, first_name: 'George'))
      karen = create(:user, first_name: 'Karen', last_name: 'Rudy')
      notification = post.notif_new_clap(karen)
      expect(notification.subject_line).to eq('Karen Rudy clapped your post')
    end
  end
end
