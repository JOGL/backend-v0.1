# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Role, type: :model do
  it { should have_and_belong_to_many(:users) }

  it { should validate_inclusion_of(:resource_type).in_array(Rolify.resource_types) }

  let!(:user) { create(:user) }

  it 'should not approve incorrect roles' do
    user.add_role :admin
    expect(user.has_role?(:moderator)).to be false
  end

  it 'should approve correct roles' do
    user.add_role :admin
    expect(user.has_role?(:admin)).to be true
  end
end
