# frozen_string_literal: true

require 'rails_helper'

require_relative 'shared_examples/mediumable'
require_relative 'shared_examples/feedable'

RSpec.describe Workgroup, type: :model do
  it_behaves_like 'a model that has many media', :workgroup
  it_behaves_like 'a model that can have a feed', :workgroup

  it { should belong_to(:creator) }

  it { should have_and_belong_to_many(:workgroup_tags) }
  it { should have_and_belong_to_many(:interests) }
  it { should have_and_belong_to_many(:skills) }

  it { should have_many(:challenges_workgroups) }
  it { should have_many(:externalhooks) }

  it { should validate_uniqueness_of(:short_title) }

  it 'has a valid factory' do
    user = create(:user)
    workgroup = create(:workgroup, creator_id: user.id)

    expect(workgroup).to be_valid
  end

  it 'has the correct frontend link' do
    user = create(:user)
    workgroup = create(:workgroup, creator_id: user.id)
    frontend_link = workgroup.frontend_link

    expect(frontend_link).to eq("/workgroup/#{workgroup.id}")
  end

  describe 'includes utils module' do
    context 'methods should be defined inside the module' do
      it 'responds to #banner_url_sm' do
        expect(described_class.new).to respond_to(:banner_url_sm)
        expect { described_class.new.banner_url_sm }.to_not raise_error
      end

      it 'responds to #members_count' do
        expect(described_class.new).to respond_to(:members_count)
        expect { described_class.new.members_count }.to_not raise_error
      end

      it 'responds to #users_sm' do
        expect(described_class.new).to respond_to(:users_sm)
        expect { described_class.new.users_sm }.to_not raise_error
      end
    end

    it 'responds to #feed_id' do
      expect(described_class.new).to respond_to(:feed_id)
    end
  end
end
