# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Faq, type: :model do
  it { should have_many(:documents) }

  it 'has a valid factory' do
    expect(build(:faq)).to be_valid
  end

  it 'has the ability to attach documents' do
    document = create(:document)
    faq = create(:faq)
    count = faq.documents.count
    faq.documents << document

    expect(faq.documents.count).to eq(count + 1)

    expect(faq.documents.pluck(:id)).to include document.id
  end

  it 'can be attached to a program' do
    program = create(:program)
    faq = create(:faq)
    program.faq = faq

    expect(program.faq).to be faq

    expect(faq.faqable).to be program
  end
end
