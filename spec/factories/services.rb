# frozen_string_literal: true

FactoryBot.define do
  factory :service do
    creator { create(:user) }
    name { Service.names.keys.first }
    serviceable_id { create(:space).id }
    serviceable_type { 'Space' }
    token { FFaker::Guid.guid.to_s.upcase }
  end
end
