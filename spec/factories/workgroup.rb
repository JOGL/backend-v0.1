# frozen_string_literal: true

FactoryBot.define do
  factory :workgroup do
    creator

    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    logo_url { FFaker::Avatar.image }
    description { FFaker::DizzleIpsum.paragraphs }
    short_description { FFaker::DizzleIpsum.paragraph }

    latitude { rand(-90.000000000...90.000000000) }
    longitude { rand(-180.000000000...180.000000000) }

    status { 0 }

    after :create do |workgroup|
      skills = create_list(:skill, 5)
      skills.map do |skill|
        workgroup.skills << skill # has_many
      end
      ressources = create_list(:ressource, 5)
      ressources.map do |ressource|
        workgroup.ressources << ressource # has_many
      end
      workgroup.creator.add_role :member, workgroup
      workgroup.creator.add_role :admin, workgroup
      workgroup.creator.add_role :owner, workgroup
    end
  end

  factory :public_workgroup, parent: :workgroup do
    is_private { false }
  end

  factory :private_workgroup, parent: :workgroup do
    is_private { true }
  end

  factory :archived_workgroup, parent: :project do
    status { 1 }
  end
end
