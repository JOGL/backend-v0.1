# frozen_string_literal: true

FactoryBot.define do
  factory :post do
    feed
    user
    from { user }
    media { FFaker::Avatar.image }
    content { FFaker::DizzleIpsum.paragraph }
  end
end
