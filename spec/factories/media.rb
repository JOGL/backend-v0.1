FactoryBot.define do
  factory :medium do
    title { FFaker::Name.first_name }
    alt_text { FFaker::Name.first_name }
    item { Rack::Test::UploadedFile.new("#{Rails.root}/spec/test-image.png", 'image/png') }
    for_project

    trait :for_project do
      association :mediumable, factory: :project
    end

    trait :for_workgroup do
      association :mediumable, factory: :workgroup
    end

    trait :for_program do
      association :mediumable, factory: :program
    end

    trait :for_challenge do
      association :mediumable, factory: :challenge
    end

    trait :for_space do
      association :mediumable, factory: :space
    end
    
    trait :for_dataset do
      association :mediumable, factory: :dataset
    end
  end
end
