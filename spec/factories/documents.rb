# frozen_string_literal: true

FactoryBot.define do
  factory :document do
    title { FFaker::Movie.title }
    content { FFaker::DizzleIpsum.paragraph }
    after :create do |document|
      users = create_list(:user, rand(1..5))
      users.map do |user|
        document.users << user
      end
    end
  end
end
