# frozen_string_literal: true

FactoryBot.define do
  factory :program do
    description { FFaker::DizzleIpsum.sentence }
    title { FFaker::Movie.unique.title }
    short_description { FFaker::Movie.title }
    short_title { FFaker::Internet.unique.user_name }

    after :create do |program|
      user = create(:confirmed_user)
      user.add_role :member, program
      user.add_role :admin, program
      user.add_role :owner, program
    end
  end
end
