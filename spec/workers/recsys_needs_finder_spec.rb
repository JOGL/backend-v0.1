# frozen_string_literal: true

require 'rails_helper'

RSpec.describe RecsysNeedsFinder do

  describe '#latest_needs' do
    before do
      @user = create(:user)
      @needs = [ create(:need), create(:need), create(:need), create(:need), create(:need) ]
    end

    let(:recsys_needs_finder) do
      recsys_needs_finder = RecsysNeedsFinder.new(@user.id)
    end

    it 'should return an array with 5 elements' do
      expect(recsys_needs_finder.latest_needs).to be_a(Array)
      expect(recsys_needs_finder.latest_needs).not_to be_empty
      expect(recsys_needs_finder.latest_needs.size).to eq(5)
    end
  end

  describe '#list_recommended_needs' do
    before do
      @user = create(:user)

      @need_1 = create(:need, title: "NEED 1")
      @need_2 = create(:need, title: "NEED 2")
      @need_3 = create(:need, title: "NEED 3")
      create(:recsys_result,
        value: 10.0,
        sourceable_node: @user,
        targetable_node: @need_1)
      create(:recsys_result,
        value: 20.0,
        sourceable_node: @user,
        targetable_node: @need_3)

    end

    let(:subject) do
      RecsysNeedsFinder.new(@user.id)
    end

    # it 'should be sorted' do
    #   recommended_needs = subject.list_recommended_needs
    #   expect(recommended_needs.length).to eq(2)
    #   expect(recommended_needs.last).to eq(@need_1)
    #   expect(recommended_needs.first).to eq(@need_3)
    # end

    it "should not include needs that don't have a result" do
      expect(subject.list_recommended_needs).to_not include(@need_2)
    end

  end

  describe '#get_score' do
    context 'should return scores' do
      before do
        @user = create(:user)
        @needs = [ create(:need), create(:need), create(:need), create(:need) ]
      end

      let(:result) do
        RecsysNeedsFinder.new(@user.id)
      end

      it "should return an Hash" do
        expect(result.get_score).to be_a(Hash)
      end

      it "needs should have a score" do
        score = @needs.map { |need| result.get_score || 0.0 }
        expect(score).to be_truthy
        expect(score.size).to eq(4)
      end
    end
  end

  describe '#call' do
    context 'testing the final method'
    before do
      @user = create(:user)
    end

    let(:subject) do
      RecsysNeedsFinder.new(@user.id)
    end

    it "doesn't send a mail if user has seen recommended needs already" do
      # create some recsys results
      # @need_1 = create(:need, title: "NEED 1")
      # @need_2 = create(:need, title: "NEED 2")
      # @need_3 = create(:need, title: "NEED 3")

      # create(:recsys_result,
      #   value: 20.0,
      #   sourceable_node: @user,
      #   targetable_node: @need_1,
      #   relation_type: 'has_seen')
      # # create some has_seen edges
      # needs = [ @need_1, @need_2, @need3 ]
      # byebug
      # create(:recsys_datum,
      #   source_node_type: ,
      #   target_node_type: '@need1',
      #   relation_type: 'has_seen')
      # edges = needs.each { |need| @user.add_edge(need, 'has_seen') }
      # # .call
      # expect(subject.call).to receive(:recsys_result).with(edges)
      # assert that only unseen recs were sent
    end

    it "sends a mail if there is a recommended need the user hasn't seen"
    # TODO:
    # lookup how to stub RecsysMailer
    # lookup how to assert a method was called
    # It 'should record an has_seen relation for the 2 needs_ids sent to user' do

  end

end
