# coding: utf-8
# frozen_string_literal: true

require 'rails_helper'
# require 'sidekiq/testing'

RSpec.describe RecsysWorker, type: :worker do
  describe 'testing recsys recommendation' do

    before do
      ENV['JOGL_NOTIFICATIONS_EMAIL'] = 'test@jogl.io'
      @to_user = create(:user)
      @needs = [create(:need), create(:need), create(:need)]
      @campaign_id = 'user-need-20201120'
      @scores = []
    end

    it 'should send an email from JOGL test' do
      mail = RecsysMailer.recsys_recommendation(@to_user, @campaign_id, @needs, @scores)
      expect(mail.to).to eq [@to_user.email]
    end

    it 'should render the suject "We\'ve been thinking about you! 👋"' do
      mail = RecsysMailer.recsys_recommendation(@to_user, @campaign_id, @needs, @scores)
      expect(mail.subject).to eq("We've been thinking about you! 👋")
    end

    it 'should send 3 needs' do
      if defined?(needs)
        mail = RecsysMailer.recsys_recommendation(@to_user, @campaign_id, @needs, @scores)
        expect(mail.needs).to eq(3)
      end
    end
  end
end
