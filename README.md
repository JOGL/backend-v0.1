# JOGL BACKEND

Gitlab [![pipeline status](https://gitlab.com/JOGL/backend-v0.1/badges/develop/pipeline.svg)](https://gitlab.com/JOGL/backend-v0.1/-/commits/develop)
[![coverage report](https://gitlab.com/JOGL/backend-v0.1/badges/develop/coverage.svg)](https://gitlab.com/JOGL/backend-v0.1/-/commits/develop)

Skylight [![View performance data on Skylight](https://badges.skylight.io/typical/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/problem/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/rpm/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)
[![View performance data on Skylight](https://badges.skylight.io/status/nv7aSx9ZHCsi.svg)](https://oss.skylight.io/app/applications/nv7aSx9ZHCsi)

<hr />

JOGL is a **100%** open-source application built with [Ruby on Rails](https://rubyonrails.org/).

We fully welcome contributions to the code!
Feel free to browse through the [issues](https://gitlab.com/JOGL/JOGL/-/issues).
If you want to contribute, fork from the branch `develop` and merge request to the branch `develop`!

For more detailed information, please read our guide to [contributing](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).

## DEVELOP

JOGL relies on various [environment variables](https://github.com/bkeepers/dotenv/) in order to operate correctly.
We suggest creating a `.env.local` file with at least the following:

```
ALGOLIA_APP_ID=
ALGOLIA_API_TOKEN=
BACKEND_URL=http://localhost:3001
DEVISE_JWT_SECRET_KEY=
EMAIL=no-reply@jogl.io
FRONTEND_URL=http://localhost:3000
```

You will need to sign up for a free [Algolia](https://www.algolia.com/) account in order to enter the credentials above.

### LANGUAGES

We are currently using Ruby [3.0.x](https://www.ruby-lang.org/en/downloads/) and suggest using a tool like [rbenv](https://www.ruby-lang.org/en/downloads/) to manage Ruby versions.

We use Node [14.x](https://www.ruby-lang.org/en/downloads/) for a few dependencies and suggest using a tool like [nvm](https://github.com/nvm-sh/nvm) to manage Node versions.

### EXTERNAL SERVICES

JOGL relies on a few external services in order to function.

#### POSTGRES

You can install [PostgreSQL](https://www.postgresql.org/) on your operating system or via Docker:

```
%> docker run --name postgres -e POSTGRES_USER=jogl -e POSTGRES_PASSWORD=password -p 5432:5432 -d jogl_development
```

That will create a jogl_development database as well as a jogl super user. If you ever need to access Postgres, you can use [psql](http://postgresguide.com/utilities/psql.html).

```
%> psql -h localhost -d jogl_development -U jogl
# OR #
%> docker exec -it postgres psql -d jogl_development -U jogl
```

#### REDIS

You can install [Redis](https://redis.io/) on your operating system or via Docker:

```
%> docker run --name redis -p 6379:6379 -d redis
```

If you need to access Redis, you can use [redis-cli](https://redis.io/topics/rediscli).

```
%> redis-cli
# OR #
%> docker exec -it redis redis-cli
```

#### MAILDEV

You can install [MailDev](https://github.com/maildev/maildev) on your operating system or via Docker:

```
%> docker run --name maildev -p 1080:80 -p 1025:25 -d maildev/maildev
```

Visit [http://localhost:1080](http://localhost:1080) in order to view the captured SMTP traffic.

## RUN

```
%> bundle exec rails s
```

Run [Sidekiq](https://sidekiq.org/) workers:

```
%> bundle exec sidekiq -C ./config/sidekiq.yml
```

## DOCUMENT

We use [yard](https://github.com/lsegal/yard/) for documenting our codebase.

```
%> bundle exec yardoc -m markdown -M redcarpet app/**/*.rb
```

We use [graphdoc](https://github.com/2fd/graphdoc) for documenting our GraphQL schema.

```
%> bundle exec rails graphql:schema:json
%> npx graphdoc -s doc/graphql/schema.json -o doc/graphql/schema
```

## TEST

We use [RSpec](https://rspec.info/).

```
%> bundle exec rspec
```

Run tests in parallel using [parallel_tests](https://github.com/grosser/parallel_tests):

```
%> bundle exec rake parallel:setup
%> bundle exec rake parallel:spec
```

## LINT

Check used gems for any known security vulnerabilities:

```
%> bundle exec bundler-audit
```

We use [RuboCop](https://github.com/rubocop/rubocop) for checking code quality and formatting.
Check [.rubocop.yml](https://gitlab.com/JOGL/backend-v0.1/-/blob/develop/.rubocop.yml) for our configuration.

```
%> bundle exec rubocop
```

Format a single file:

```
%> bundle exec rubocop --fix-layout file.rb 
```
