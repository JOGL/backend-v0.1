# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.2'

group :development do
  gem 'rack-mini-profiler', '2.3.2'
end

gem 'active_model_serializers', '0.10.12'
gem 'algoliasearch-rails', '2.1.0'
gem 'attr_json', '1.3.0'
gem 'aws-sdk-s3', '1.96.1', require: false
gem 'bcrypt', '3.1.16'
gem 'composite_primary_keys', '13.0.0'
gem 'devise', '4.8.0'
gem 'devise-async', '1.0.0'
gem 'devise-jwt', '0.9.0'
gem 'et-orbi', '1.2.4'
gem 'faraday', '1.5.1'
gem 'faraday_middleware', '1.0.0'
gem 'geocoder', '1.6.7'
gem 'gibbon', '3.4.0'
gem 'graphql', '1.12.13'
gem 'hashie', '3.6.0'
gem 'image_processing', '1.12.1'
gem 'logstasher', '2.1.5'
gem 'logstash-logger', '0.26.1'
gem 'merit', '4.0.2'
gem 'migration_data', '0.6.0'
gem 'mini_magick', '4.11.0'
gem 'mjml-rails', '4.6.1'
gem 'multipart-post', '2.1.1'
gem 'notification-pusher-actionmailer', '2.0.0'
gem 'notifications-rails', '2.0.0'
gem 'oj', '3.11.8'
gem 'omniauth', '2.0.4'
gem 'omniauth-eventbrite', '0.0.6'
gem 'pagy', '4.10.1'
gem 'panko_serializer', '0.7.5'
gem 'paper_trail', '12.0.0'
gem 'pg', '1.2.3'
gem 'platform-api', '3.3.0'
gem 'postmark-rails', '0.21.0'
gem 'puma', '5.5.1'
gem 'rack-cors', '1.1.1', require: 'rack/cors'
gem 'rails', '6.1.4.1'
gem 'redis', '4.3.1'
gem 'rolify', '6.0.0'
gem 'seedbank', '0.5.0'
gem 'sentry-rails', '4.6.1'
gem 'sentry-ruby', '4.6.1'
gem 'sentry-ruby-core', '4.6.1'
gem 'sentry-sidekiq', '4.6.1'
gem 'sidekiq', '6.2.1', require: %w[sidekiq sidekiq/web]
gem 'sidekiq-cron', '1.2.0'
gem 'slack-notifier', '2.4.0'

group :development do
  gem 'bundler-audit', '0.8.0'
  gem 'listen', '3.5.1'
  gem 'rbs', '1.2.1'
  gem 'solargraph', '0.42.4'
  gem 'spring', '2.1.1'
  gem 'spring-commands-rspec', '1.0.4'
  gem 'spring-watcher-listen', '2.0.1'
  gem 'steep', '0.44.1'
end

group :development, :test do
  gem 'bullet', '6.1.4'
  gem 'byebug', '11.1.3', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails', '2.7.6'
  gem 'factory_bot_rails', '6.2.0'
  gem 'ffaker', '2.18.0', require: true
  gem 'parallel_tests', '3.7.0'
  gem 'pry', '0.13.1'
  gem 'pry-byebug', '3.9.0'
  gem 'pry-rails', '0.3.9'
  gem 'redcarpet', '3.5.1'
  gem 'rubocop', '1.18.3'
  gem 'rubocop-performance', '1.11.4'
  gem 'rubocop-rails', '2.11.3'
  gem 'rubocop-rspec', '2.4.0'
  gem 'yard', '0.9.26'
end

group :production do
  gem 'coverband', '5.1.0'
  gem 'skylight', '5.1.1'
end

group :test do
  gem 'database_cleaner', '2.0.1'
  gem 'rails-controller-testing', '1.0.5'
  gem 'rspec-rails', '5.0.1'
  gem 'rspec-sidekiq', '3.1.0'
  gem 'shoulda', '4.0.0'
  gem 'simplecov', '0.21.2', require: false
  gem 'sinatra', '2.1.0'
  gem 'timecop', '0.9.4'
  gem 'webmock', '3.13.0'
end
