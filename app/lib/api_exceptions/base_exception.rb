# frozen_string_literal: true

module ApiExceptions
  class BaseException < StandardError
    attr_reader :data, :status

    def as_json
      {
        data: data
      }
    end
  end
end
