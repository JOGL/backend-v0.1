# frozen_string_literal: true

class UserJoinedMailer < ApplicationMailer
  def user_joined(to, object, joiner)
    @joiner = joiner
    @object = object
    @object_type = object.class.name.casecmp('workgroup').zero? ? 'group' : object.class.name.downcase
    @to = to

    # Postmark metadatas
    metadata['user-id-to'] = to.id
    metadata['object-type'] = @object_type

    mail(
      to: "<#{to.email}>",
      from: "JOGL - Just One Giant Lab <notifications@#{Rails.configuration.email}>",
      subject: "A user joined your #{@object_type}",
      tag: 'user-joined'
    )
  end
end
