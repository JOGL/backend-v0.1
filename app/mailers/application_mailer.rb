# frozen_string_literal: true

require 'digest/sha2'

class ApplicationMailer < ActionMailer::Base
  layout 'base_mailer'

  default from: "JOGL - Just One Giant Lab <contact@#{Rails.configuration.email}>",
          message_id: "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@#{Rails.configuration.email}>"
end
