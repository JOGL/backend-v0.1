# frozen_string_literal: true

class GeocoderWorker
  include Sidekiq::Worker

  def perform(object_type, object_id)
    @object = object_type.singularize.classify.constantize.find(object_id)
    @results = []
    @searches = %i[search_city search_address search_ip]

    while @results.empty? && @searches.any?
      search = @searches.pop
      method(search).call
    end

    return if @results.empty?

    set_latitude_longitude
    @object.save!
  end

  private

  def search_ip
    @results = Geocoder.search(@object.ip) if @object.instance_of?(User) && @object.city.nil?
  end

  def search_address
    criteria = [@object.address, @object.city, @object.country].compact.join(', ')
    @results = Geocoder.search(criteria)
  end

  def search_city
    criteria = [@object.city, @object.country].compact.join(', ')
    @results = Geocoder.search(criteria)
  end

  def set_latitude_longitude
    @object.latitude = @results.first.coordinates[0]
    @object.longitude = @results.first.coordinates[1]
  end
end
