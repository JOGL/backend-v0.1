# frozen_string_literal: true

require 'concerns/graph.rb'

class NetworkMakerWorker
  include Sidekiq::Worker

  def perform
    @G = Graph.new('JOGL database directed graph', 'DiGraph', directed = true, metadata = { created_at: Time.now })

    # Creating all the nodes

    Skill.all.each do |skill|
      @G.add_node(skill,
                  skill.skill_name)
    end

    Ressource.all.each do |ressource|
      @G.add_node(ressource,
                  ressource.ressource_name)
    end

    Interest.all.each do |interest|
      @G.add_node(interest,
                  interest.interest_name)
    end

    User.all.each do |user|
      @G.add_node(user,
                  user.nickname,
                  metadata = {
                    first_name: user.first_name,
                    last_name: user.last_name,
                    status: user.active_status,
                    created_at: user.created_at,
                    updated_at: user.updated_at
                  })
    end

    Post.all.each do |post|
      @G.add_node(post,
                  post.id,
                  metadata = { created_at: post.created_at, updated_at: post.updated_at })
    end

    Comment.all.each do |comment|
      @G.add_node(comment,
                  comment.id,
                  metadata = { created_at: comment.created_at, updated_at: comment.updated_at })
    end

    Need.all.each do |need|
      @G.add_node(need,
                  need.title,
                  metadata = { created_at: need.created_at,
                               updated_at: need.updated_at })
    end

    Project.all.each do |project|
      @G.add_node(project,
                  project.title,
                  metadata = {
                    short_title: project.short_title,
                    short_description: project.short_description,
                    status: project.status,
                    created_at: project.created_at,
                    updated_at: project.updated_at
                  })
    end

    Workgroup.all.each do |workgroup|
      @G.add_node(workgroup,
                  workgroup.title,
                  metadata = {
                    short_title: workgroup.short_title,
                    short_description: workgroup.short_description,
                    status: workgroup.status,
                    created_at: workgroup.created_at,
                    updated_at: workgroup.updated_at
                  })
    end

    Challenge.all.each do |challenge|
      @G.add_node(challenge,
                  challenge.title,
                  metadata = {
                    short_title: challenge.short_title,
                    short_description: challenge.short_description,
                    status: challenge.status,
                    created_at: challenge.created_at,
                    updated_at: challenge.updated_at
                  })
    end

    Program.all.each do |program|
      @G.add_node(program,
                  program.title,
                  metadata = {
                    short_title: program.short_title,
                    short_description: program.short_description,
                    status: program.status,
                    created_at: program.created_at,
                    updated_at: program.updated_at
                  })
    end

    RecsysDatum.all.each do |edge|
      source = edge.sourceable_node
      target = edge.targetable_node
      next if source.nil? || target.nil?

      @G.add_edge(source, target, edge.relation_type, true, metadata = {
                    created_at: edge.created_at,
                    updated_at: edge.updated_at,
                    value: edge.value
                  })
    end

    # @G.render
    network = Network.create(json_network: @G.render)
    network.save!
  end
end
