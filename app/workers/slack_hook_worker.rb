# frozen_string_literal: true

class SlackHookWorker
  include Sidekiq::Worker

  def perform(slackhook_id, action, object_type, object_id, secondary_object_type = nil, secondary_object_id = nil)
    @slackhook = Slackhook.find(slackhook_id)

    @obj = object_type.constantize.find(object_id)
    @obj2 = secondary_object_type.constantize.find(secondary_object_id) unless secondary_object_type.nil?

    if object_type == 'Need'
      if action == 'new'
        attachment = new_need_formater
        message = 'A new need has been created !'
      end
    elsif object_type == 'Post'
      @user = User.find(@obj.user_id)
      if action == 'new'
        attachment = new_post_formater
        message = @user.first_name + ' ' + @user.last_name + ' just posted something!'
      end
    elsif object_type == 'Project'
      @user = User.find(@obj.creator_id)
      if action == 'attach'
        attachment = attach_project_formater
        message = @obj.title + ' wants to join the challenge!'
      end
    elsif object_type == 'User'
      if action == 'join'
        attachment = join_user_formater
        message = @obj.first_name + ' ' + @obj.last_name + ' just joined ' + @obj2.title + '!'
      end
    end

    notifier = Slack::Notifier.new @slackhook.hook_url, channel: @slackhook.channel, username: @slackhook.username

    notifier.post text: message, attachments: [attachment]
  end

  def new_need_formater
    {
      fallback: 'A new need has been created!',
      color: '#36a64f',
      # pretext: "Optional text that appears above the attachment block",
      author_name: 'From:' + @obj.project.title,
      author_link: Rails.configuration.frontend_url + '/project/' + @obj.project.id.to_s,
      # author_icon: "http://flickr.com/icons/bobby.jpg",
      title: @obj.title,
      title_link: Rails.configuration.frontend_url + '/project/' + @obj.project.id.to_s + '/needs/' + @obj.id.to_s,
      text: @obj.content[0...100] + '[...]',
      fields: [
        {
          title: 'Skills needed',
          value: skills,
          short: true
        }
      ],
      image_url: 'https://app.jogl.io',
      thumb_url: 'https://app.jogl.io',
      footer: 'Just One Giant Lab',
      footer_icon: 'https://app.jogl.io/favicon.ico'
    }
  end

  def join_user_formater
    {
      fallback: 'Someone joined an object!',
      color: '#36a64f',
      # pretext: "Optional text that appears above the attachment block",
      author_name: 'From:' + @obj2.title,
      author_link: Rails.configuration.frontend_url + '/' + @obj2.class.name.downcase + '/' + @obj.id.to_s,
      # author_icon: "http://flickr.com/icons/bobby.jpg",
      title: @obj.first_name + ' ' + @obj.last_name,
      title_link: Rails.configuration.frontend_url + '/user/' + @obj.id.to_s,
      text: @obj.short_bio.nil? ? 'No bio... :(' : @obj.short_bio[0...100] + '[...]',
      fields: [
        {
          title: 'Skills',
          value: skills,
          short: true
        }
      ],
      image_url: 'https://app.jogl.io',
      thumb_url: 'https://app.jogl.io',
      footer: 'Just One Giant Lab',
      footer_icon: 'https://app.jogl.io/favicon.ico'
    }
  end

  def new_post_formater
    {
      fallback: 'Someone posted something new!',
      color: '#36a64f',
      # pretext: "Optional text that appears above the attachment block",
      author_name: 'Go read it!',
      author_link: Rails.configuration.frontend_url + '/post/' + @obj.id.to_s,
      # author_icon: "http://flickr.com/icons/bobby.jpg",
      title: 'From: ' + @user.first_name + ' ' + @user.last_name,
      title_link: Rails.configuration.frontend_url + '/user/' + @user.id.to_s,
      text: @obj.content[0...100] + '[...]',
      image_url: 'https://app.jogl.io',
      thumb_url: 'https://app.jogl.io',
      footer: 'Just One Giant Lab',
      footer_icon: 'https://app.jogl.io/favicon.ico'
    }
  end

  def attach_project_formater
    {
      fallback: 'A project wants to join a challenge!',
      color: '#36a64f',
      # pretext: "Optional text that appears above the attachment block",
      author_name: @obj.title,
      author_link: Rails.configuration.frontend_url + '/project/' + @obj.id.to_s,
      # author_icon: "http://flickr.com/icons/bobby.jpg",
      title: 'Creator: ' + @user.first_name + ' ' + @user.last_name,
      title_link: Rails.configuration.frontend_url + '/user/' + @user.id.to_s,
      text: @obj.short_description[0...100] + '[...]',
      fields: [
        {
          title: 'Skills needed',
          value: skills,
          short: true
        }
      ],
      image_url: 'https://app.jogl.io',
      thumb_url: 'https://app.jogl.io',
      footer: 'Just One Giant Lab',
      footer_icon: 'https://app.jogl.io/favicon.ico'
    }
  end

  def skills
    result = []
    @obj.skills.first(5).map do |skill|
      result << skill.skill_name
    end
    result.join(', ')
  end
end
