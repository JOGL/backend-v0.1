# frozen_string_literal: true

class UserJoinedEmailWorker
  include Sidekiq::Worker

  def perform(object_type, object_id, joiner_id)
    joiner = User.find(joiner_id)
    object = object_type.constantize.find(object_id)

    object.owners.each do |owner|
      UserJoinedMailer.user_joined(owner, object, joiner).deliver unless owner.deleted?
    end
  end
end
