# frozen_string_literal: true

class ResetDirectMessageCounts
  include Sidekiq::Worker

  def perform
    User.update_all(direct_message_count: 0)
  end
end
