# frozen_string_literal: true

class CleanDeadUsers
  include Sidekiq::Worker

  def perform
    # users created more than a month ago that didn't confirm
    User
      .where(confirmed_at: nil)
      .where("created_at < NOW() - INTERVAL '30 days'")
      .each(&:archived!)
  end
end
