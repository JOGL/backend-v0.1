# frozen_string_literal: true

module Api
  module Customfields
    extend ActiveSupport::Concern

    included do
      before_action :customfield_authenticate_user!, only: %i[create_custom_data create_custom_field update_custom_data update_custom_field
                                                              delete_custom_data delete_custom_field get_custom_data get_my_custom_datas]
    end

    def create_custom_field
      field = @obj.customfields.create(validate_fields)
      if field.nil?
        render json: { data: 'an error as occured' }, status: :unprocessable_entity
      else
        render json: { data: 'Fields created' }, status: :created
      end
    end

    def update_custom_field
      @field = Customfield.find(params[:field_id])
      if @field.nil?
        render json: { data: 'Field ot found' }, status: :not_found
      else
        if @field.update(validate_fields)
          render json: { data: 'Fields updated' }, status: :ok
        else
          render json: { data: 'an error as occured' }, status: :unprocessable_entity
        end
      end
    end

    def delete_custom_field
      @field = Customfield.find(params[:field_id])
      if @field.nil?
        render json: { data: 'Missing field id to update' }, status: :unprocessable_entity
      else
        # Delete the data associated first
        @field.customdatas.all.map(&:delete)
        # Then delete the field itself
        if @field.delete
          render json: { data: 'Fields deleted' }, status: :ok
        else
          render json: { data: 'an error as occured' }, status: :unprocessable_entity
        end
      end
    end

    def get_custom_fields
      render json: @obj.customfields, each_serializer: Api::CustomfieldSerializer
    end

    def create_custom_data
      @customfield = Customfield.find(params[:field_id])
      if @customfield.nil?
        render json: { data: 'Customfield ' + params[:field_id] + ' not found' }, status: :not_found
      else
        @customfield.customdatas.create(user_id: current_user.id, value: validate_data[:value])
        render json: { data: 'Data entered' }, status: :created
      end
    end

    def update_custom_data
      @customfield = Customfield.find(params[:field_id])
      if @customfield.nil?
        render json: { data: 'Customfield ' + params[:field_id] + ' not found' }, status: :not_found
      else
        @customdata = @customfield.customdatas.find_by(user_id: current_user.id)
        if @customdata.nil?
          render json: { data: 'Customdata not found' }, status: :not_found
        else
          if @customdata.update(validate_data)
            render json: { data: 'Data updated' }, status: :ok
          else
            render json: { data: 'an error as occured' }, status: :unprocessable_entity
          end
        end
      end
    end

    def get_custom_data
      @customfield = Customfield.find(params[:field_id])
      render json: @customfield.customdatas, each_serializer: Api::CustomdataSerializer
    end

    def get_my_custom_datas
      answers = []
      if current_user.nil?
        render json: { data: 'Forbidden' }, status: :forbidden
      else
        @obj.customfields.map do |customfield|
          answer = customfield.customdatas.find_by(user_id: current_user.id)
          answers << answer unless answer.nil?
        end
        render json: answers, each_serializer: Api::CustomdataSerializer
      end
    end

    def validate_fields
      params.require(:fields).permit(:description, :name, :field_type, :optional)
    end

    def validate_data
      params.require(:data).permit(:value)
    end
  end
end
