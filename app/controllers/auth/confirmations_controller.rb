# frozen_string_literal: true

module Auth
  class ConfirmationsController < Devise::ConfirmationsController
    def new
      super
    end

    def create
      super
    end

    def show
      self.resource = resource_class.confirm_by_token(params[:confirmation_token])

      render status: :unprocessable_entity and return unless resource.errors.empty?

      redirect_to "#{ENV['FRONTEND_URL']}/signin?confirmed=true"
    end
  end
end
