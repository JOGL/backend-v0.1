# frozen_string_literal: true

module Api
  class PeerReviewsController < ApplicationController
    before_action :authenticate_user!, only: %i[create destroy my_peer_reviews update]
    before_action :load_peer_review, only: %i[destroy email_admins email_admins_single email_users email_reviewers faq_create faq_index faq_update faq_destroy get_reviewers_info get_admins_info index_people index_proposals
                                              invite invite_as_admin join leave
                                              members_list remove_banner remove_member remove_proposal show update update_member upload_banner]
    before_action :is_admin,
                  only: %i[destroy email_admins email_admins_single email_users email_reviewers faq_destroy get_reviewers_info get_admins_info invite invite_as_admin remove_banner remove_member remove_proposal update update_member
                           upload_banner]

    include Api::Faq
    include Api::Members
    include Api::Relations
    include Api::Upload
    include Api::Utils

    def create
      peer_review = PeerReview.new(peer_review_params)

      if peer_review.save
        peer_review.update_interests(params[:peer_review][:interests]) unless params[:peer_review][:interests].nil?
        peer_review.update_skills(params[:peer_review][:skills]) unless params[:peer_review][:skills].nil?

        current_user.add_role :owner, peer_review
        current_user.add_role :admin, peer_review

        render json: peer_review, status: :created
      else
        render json: peer_review.errors.full_messages, status: :unprocessable_entity
      end
    end

    def destroy
      @peer_review.destroy

      render status: :ok
    end

    def email_admins
      params.permit(:content, :subject)

      @peer_review.proposals.each do |proposal|
        User.with_role(:admin, proposal).each do |user|
          PeerReviewEmailWorker.perform_async(current_user.id, user.id, @peer_review.id, params[:content], params[:subject])
        end
      end

      render status: :accepted
    end

    def email_admins_single
      params.permit(:content, :subject, :proposal_ids)

      @peer_review.proposals.each do |proposal|
        if params[:proposal_ids].include?(proposal.id)
          User.with_role(:admin, proposal).each do |user|
            PeerReviewEmailWorker.perform_async(current_user.id, user.id, @peer_review.id, params[:content], params[:subject])
          end
        end
      end

      render status: :accepted
    end

    def email_users
      params.permit(:content, :subject, :user_ids)

      params[:user_ids].each do |user_id|
        PeerReviewEmailWorker.perform_async(current_user.id, user_id, @peer_review.id, params[:content], params[:subject])
      end

      render status: :accepted
    end

    def email_reviewers
      params.permit(:content, :subject)

      members = User.with_role(:member, @peer_review)

      @peer_review.proposals.each do |proposal|
        members += User.with_role(:admin, proposal)
        members += User.with_role(:member, proposal)
      end

      members.uniq(&:id).each do |user|
        PeerReviewEmailWorker.perform_async(current_user.id, user.id, @peer_review.id, params[:content], params[:subject])
      end

      render status: :accepted
    end

    def get_admins_info
      members = []
      @peer_review.proposals.each do |proposal|
        User.with_role(:admin, proposal).each do |user|
          members << user
        end
      end

      respond_to do |format|
        format.csv do
          send_data User.to_csv(members.uniq(&:id)), filename: "admins-info-#{Time.zone.today}.csv"
        end

        format.json do
          render json: {
            users: members.uniq(&:id).map { |user| Api::UserSerializer.new(user) }
          }, status: :ok
        end
      end
    end

    def get_reviewers_info
      members = User.with_role(:member, @peer_review)

      @peer_review.proposals.each do |proposal|
        members += User.with_role(:admin, proposal)
        members += User.with_role(:member, proposal)
      end

      respond_to do |format|
        format.csv do
          send_data User.to_csv(members.uniq(&:id)), filename: "reviewers-info-#{Time.zone.today}.csv"
        end

        format.json do
          render json: {
            users: members.uniq(&:id).map { |user| Api::UserSerializer.new(user) }
          }, status: :ok
        end
      end
    end

    def index
      peer_reviews = if params[:program_id]
                       Program.find(params.require(:program_id)).peer_reviews
                     elsif params[:space_id]
                       Space.find(params.require(:space_id)).peer_reviews
                     else
                       PeerReview.all
                     end

      render json: peer_reviews.order(created_at: :desc), controller: true
    end

    def index_people
      members = User
                .select("users.*, 'member' as relation")
                .joins('LEFT JOIN users_roles ON users.id = users_roles.user_id')
                .joins('LEFT JOIN roles ON roles.id = users_roles.role_id')
                .where("roles.name = 'member' AND roles.resource_type = 'PeerReview' AND roles.resource_id = :peer_review_id", peer_review_id: @peer_review.id)

      proposal_participants = User
                              .select("users.*, 'participant' as relation")
                              .joins('LEFT JOIN users_roles ON users.id = users_roles.user_id')
                              .joins('LEFT JOIN roles ON roles.id = users_roles.role_id')
                              .where("roles.resource_type = 'Proposal' AND roles.resource_id IN (SELECT id FROM proposals WHERE peer_review_id = :peer_review_id)", peer_review_id: @peer_review.id)

      people = (members + proposal_participants).uniq(&:id)

      pagy = Pagy.new(count: people.count, items: params.key?(:items) ? params[:items] : 25, page: params.key?(:page) ? params[:page] : 1)

      render json: people[pagy.offset, pagy.items],
             each_serializer: Api::UserSerializer,
             root: 'people',
             adapter: :json
    end

    def index_proposals
      proposals = Proposal
                  .where(peer_review_id: @peer_review.id)
                  .order(id: :desc)

      @pagy, records = pagy(proposals)
      render json: records, each_serializer: Api::ProposalSerializer, root: 'proposals', adapter: :json
    end

    def my_peer_reviews
      peer_reviews = PeerReview.with_role(:owner, current_user)
      peer_reviews += PeerReview.with_role(:admin, current_user)
      peer_reviews += PeerReview.with_role(:member, current_user)

      render json: peer_reviews.uniq.sort_by(&:id)
    end

     # DELETE /proposals/:proposal_id
     def remove_proposal
      proposal = Proposal.find(params[:proposal_id])
      if @peer_review.proposals.include?(proposal)
        @peer_review.proposals.delete(proposal)
        render json: { data: "Proposal with id:#{proposal.id} was removed" }, status: :ok
      else
        render json: { data: "Proposal with id:#{proposal.id} is not attached" }, status: :not_found
      end
    end

    def show
      render json: @peer_review, status: :ok, controller: true
    end

    # rubocop:disable Metrics/AbcSize
    # rubocop:disable Metrics/CyclomaticComplexity
    # rubocop:disable Metrics/PerceivedComplexity
    def update
      if peer_review_params[:score_threshold] && @peer_review.score_threshold
        @peer_review.proposals.each do |proposal|
          project = Project.find(proposal.project_id)

          if proposal.score && proposal.score > @peer_review.score_threshold
            User.with_role(:reviewer).first.add_role(:reviewer, project)
          else
            User.with_role(:reviewer).first.remove_role(:reviewer, project)
          end
        end
      end

      if @peer_review.update(peer_review_params)
        @peer_review.update_interests(params[:peer_review][:interests]) unless params[:peer_review][:interests].nil?
        @peer_review.update_skills(params[:peer_review][:skills]) unless params[:peer_review][:skills].nil?

        render json: @peer_review, status: :ok, controller: true
      else
        render json: @peer_review.errors.full_messages, status: :unprocessable_entity
      end
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/CyclomaticComplexity
    # rubocop:enable Metrics/PerceivedComplexity

    private

    def is_admin
      render status: :forbidden unless current_user.has_role?(:admin, @peer_review)
    end

    def load_peer_review
      @peer_review = PeerReview.find(params.require(:id))
      @obj = @peer_review

      render status: :not_found if @peer_review.nil?
    end

    def peer_review_params
      params.require(:peer_review).permit(
        :deadline,
        :description,
        :maximum_disbursement,
        :resource_id,
        :resource_type,
        :rules,
        :score_threshold,
        :short_title,
        :start,
        :stop,
        :submitted_at,
        :summary,
        :total_funds,
        :title,
        :visibility,
        options: {}
      )
    end
  end
end
