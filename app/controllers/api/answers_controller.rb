# frozen_string_literal: true

module Api
  class AnswersController < ApplicationController
    before_action :authenticate_user!
    before_action :load_answer, only: %i[destroy update]

    def create
      answer = Answer.new(answer_params)

      if answer.save
        render json: answer, status: :created
      else
        render json: answer.errors.full_messages, status: :unprocessable_entity
      end
    end

    def destroy
      @answer.destroy

      render status: :ok
    end

    def update
      if @answer.update(answer_params)
        render @answer, status: :ok
      else
        render json: @answer.errors.full_messages, status: :unprocessable_entity
      end
    end

    private

    def answer_params
      params.require(:answer).permit(:id, :content, :document_id, :proposal_id)
    end

    def load_answer
      @answer = Answer.find(params.require(:id))

      render status: :not_found if @answer.nil?
    end
  end
end
