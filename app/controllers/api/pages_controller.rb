# frozen_string_literal: true

module Api
  class PagesController < ApplicationController
    def index
      render json: { content: 'Welcome to the JOGL API' }, status: :ok
    end
  end
end
