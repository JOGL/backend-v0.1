# frozen_string_literal: true

module Api
  class DatasetsController < ApplicationController
    before_action :authenticate_user!, except: %i[index show similar]
    before_action :set_dataset, except: %i[index create]
    before_action :set_from, only: [:create]
    before_action :set_obj, except: %i[index create destroy recommended]

    include Api::Media
    include Api::Follow
    include Api::Recommendations
    include Api::Relations
    include Api::Utils

    # GET /datasets
    def index
      @pagy, @datasets = pagy(Dataset.includes([:data, :sources, :tags]).all)
      render json: @datasets, status: :ok, include: '**'
    end

    # GET /datasets/1
    def show
      render json: @dataset, status: :ok, include: '**'
    end

    # POST /datasets/make
    def make
      @dataset = Dataset.new(dataset_params)
      @dataset.author_id = current_user.id
      @dataset.datasetable = @from
      if @dataset.valid?
        render json: @dataset, status: :ok, include: '**'
      else
        render json: @dataset.errors, status: :unprocessable_entity
      end
    end

    # POST /datasets
    def create
      @dataset = Dataset.new(dataset_params)
      @dataset.author_id = current_user.id
      @dataset.datasetable = @from
      if @dataset.save
        current_user.add_edge(@dataset, 'is_author_of')
        render json: @dataset, status: :created, include: '**'
      else
        render json: @dataset.errors, status: :unprocessable_entity
      end
    end

    # PATCH/PUT /datasets/1
    def update
      if @dataset.update(dataset_params)
        render json: @dataset, include: '**'
      else
        render json: @dataset.errors, status: :unprocessable_entity
      end
    end

    # DELETE /datasets/1
    def destroy
      @dataset.destroy
      current_user.remove_edge(@dataset, 'is_author_of')
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_dataset
      @dataset = Dataset.find(params[:id])
      render json: { data: "Dataset id:#{params[:id]} was not found" }, status: :not_found unless @dataset
    end

    def set_obj
      @obj = @dataset
    end

    def set_from
      @from = if params[:from_type]
                params[:from_type].camelize.constantize.find(params[:from_id])
              else
                current_user
              end
      render json: { data: 'Parent was not found' }, status: :not_found unless @from
    end

    # Only allow a trusted parameter "white list" through.
    def dataset_params
      # byebug
      render json: { data: 'Dataset type is incorrect' }, status: :unprocessable_entity unless ['Datasets::CkanDataset', 'Datasets::KapCodeDataset',
                                                                                                'Datasets::CustomDataset'].include? params[:dataset][:type]
      params.require(:dataset).permit(:type, :title, :description, :url)
    end
  end
end
