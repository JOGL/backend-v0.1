# frozen_string_literal: true

module Api::RolesSerializerHelper
  # Verify user role for a given object
  # Those ,methods are used by the serializer only, and are different from the methods defined
  # in the controlers concerns.
  # The methods here only serve to render a true false state for the frontend
  # so that the buttons and other actions displayed to the users are correctly rendered
  # They are not security based

  def is_owner(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:owner, obj)
    end
  end

  def is_admin(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:admin, obj) || current_user.has_role?(:admin)
    end
  end

  def is_member(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:member, obj)
    end
  end

  def is_reviewer(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:reviewer, obj) || current_user.has_role?(:reviewer)
    end
  end

  def is_pending(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:pending, obj)
    end
  end
end
