# frozen_string_literal: true

# Serialize JSONB.
class JsonbSerializer
  # Dump an object to JSON.
  #
  # @param [Object] obj any object
  # @return [JSON] the JSON representation of the object
  def self.dump(obj)
    obj.to_json
  end

  # Load a JSON string as a Hash.
  #
  # @param [String] json a JSON string
  # @return [Hash] a hash with symbol as well as string keys
  def self.load(json)
    h = JSON.parse(json) if json.instance_of?(String)
    HashWithIndifferentAccess.new(h)
  end
end
