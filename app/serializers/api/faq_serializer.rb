# frozen_string_literal: true

module Api
  class FaqSerializer < ActiveModel::Serializer
    attributes :documents

    def documents
      object.documents.order(:position)
    end
  end
end
