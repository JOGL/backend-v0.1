# frozen_string_literal: true

module Api
  class FollowersSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper

    attributes :followers

    def followers
      object.followers.map do |follower|
        follower = Users::DeletedUser.new if follower.nil? || follower.deleted?
        data = {
          id: follower.id,
          first_name: follower.first_name,
          last_name: follower.last_name,
          nickname: follower.nickname,
          mutual_count: mutual_count(follower),
          logo_url: follower.logo_url,
          logo_url_sm: follower.logo_url_sm,
          has_followed: has_followed(follower),
          has_clapped: has_clapped(follower),
          can_contact: follower.can_contact,
          short_bio: follower.short_bio
        }
        if object.instance_of?(User)
          data = data.merge({
                              projects_count: follower.projects_count,
                              spaces_count: follower.spaces_count
                            })
        end
        data
      end
    end

    def mutual_count(follower)
      if current_user.nil?
        0
      else
        follower.follow_mutual_count(current_user)
      end
    end
  end
end
