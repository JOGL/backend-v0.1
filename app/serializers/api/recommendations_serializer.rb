# frozen_string_literal: true

module Api
  class RecommendationsSerializer < ActiveModel::Serializer
    belongs_to :targetable_node, polymorphic: true
  end
end
