# frozen_string_literal: true

module Api
  class AnswerSerializer < ActiveModel::Serializer
    attributes :id,
               :content,
               :document,
               :proposal_id
  end
end
