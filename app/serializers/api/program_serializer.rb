# frozen_string_literal: true

module Api
  class ProgramSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::RolesSerializerHelper
    include Api::UsersSerializerHelper
    include Api::UtilsSerializerHelper

    attributes :id,
               :title,
               :title_fr,
               :custom_challenge_name,
               :short_title,
               :short_title_fr,
               :banner_url,
               :banner_url_sm,
               :logo_url,
               :logo_url_sm,
               :status,
               :users_sm,
               :feed_id,
               :short_description,
               :short_description_fr,
               :contact_email,
               :meeting_information,
               :ressources,
               :launch_date,
               :end_date,
               :followers_count,
               :saves_count,
               :members_count,
               :needs_count,
               :projects_count,
               :posts_count,
               :created_at,
               :updated_at

    attribute :has_followed, unless: :scope?
    attribute :has_saved, unless: :scope?
    attribute :is_admin, unless: :scope?
    attribute :is_member, unless: :scope?
    attribute :is_owner, unless: :scope?
    # Show following attributes only if show_objects is true (set in controller, usually true only for :show api call)
    attribute :description, if: :show_objects?
    attribute :description_fr, if: :show_objects?
    attribute :enablers, if: :show_objects?
    attribute :enablers_fr, if: :show_objects?

    def show_objects?
      @instance_options[:show_objects]
    end

    def ressources
      object.ressources
    end
  end
end
