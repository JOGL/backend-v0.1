# frozen_string_literal: true

module Api
  class UserSigninSerializer < ActiveModel::Serializer
    include Api::RelationsSerializerHelper
    include Api::UtilsSerializerHelper

    attributes  :id,
                :email,
                :first_name,
                :last_name,
                :nickname,
                :age,
                :category,
                :affiliation,
                :country,
                :city,
                :address,
                :phone_number,
                :bio,
                :status,
                :mail_newsletter,
                :mail_weekly,
                :sign_in_count,
                :confirmed_at,
                :interests,
                :skills,
                :feed_id,
                :geoloc,
                :logo_url,
                :logo_url_sm,
                :claps_count,
                :followers_count,
                :following_count
  end
end
