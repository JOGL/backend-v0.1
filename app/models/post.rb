# frozen_string_literal: true

class Post < ApplicationRecord
  include AlgoliaSearch
  include RecsysHelpers
  include RelationHelpers
  include Utils

  belongs_to :feed
  belongs_to :from, polymorphic: true, foreign_type: :from_object, foreign_key: :from_id
  belongs_to :user

  has_and_belongs_to_many :feeds

  has_many :comments
  has_many :mentions

  has_many_attached :documents

  enum category: { discussion: 0, announcement: 1 }

  before_create :sanitize_content
  before_update :sanitize_content

  validates :content, presence: true

  notification_object
  resourcify

  algoliasearch disable_indexing: !Rails.env.production? do
    use_serializer Api::PostSerializer
  end

  def creator
    {
      id: user.id,
      first_name: user.first_name,
      last_name: user.last_name,
      logo_url: user.logo_url_sm
    }
  end

  def users
    [user, comments.map(&:user)].flatten.uniq
  end

  def participants_except_commenter(comment)
    # user_id(post creator) + all_other_commenters - current_commenter
    participant_ids = (([user_id] + comments.pluck(:user_id)).uniq - [comment.user_id])
    User.where(id: participant_ids)
  end

  def frontend_link
    "/post/#{id}"
  end

  def notif_new_clap(clapper)
    Notification.create(
      target: user,
      category: :clap,
      type: 'new_clap',
      object: self,
      metadata: {
        clapper: clapper
      }
    )
  end
end
