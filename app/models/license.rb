# frozen_string_literal: true

class License < ApplicationRecord
  belongs_to :licenseable, polymorphic: true
end
