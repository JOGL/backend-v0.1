# frozen_string_literal: true

class Need < ApplicationRecord
  include AlgoliaSearch
  include Geocodable
  include Feedable
  include Mediumable
  include Membership
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Ressourceable
  include Skillable
  include Utils

  belongs_to :project
  belongs_to :user

  enum status: %i[active archived completed draft]

  has_many_attached :documents

  after_validation :geocode
  after_create :notif_new_need

  validates :content, presence: true
  validates :title, presence: true

  geocoded_by :make_address
  notification_object
  resourcify

  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::NeedSerializer
    add_attribute :posts_count
    geoloc :latitude, :longitude
  end

  def notif_new_need
    Notification.for_group(
      :members,
      args: [project],
      attrs: {
        category: :administration,
        type: 'new_need',
        object: self
      }
    )
  end

  def frontend_link
    "/need/#{id}"
  end
end
