# frozen_string_literal: true

class Datasets::CustomDataset < ::Dataset
  def custom
    true
  end
end
