# frozen_string_literal: true

# Application of a Project to a PeerReview.
class Proposal < ApplicationRecord
  include AlgoliaSearch
  include Interestable
  include Membership
  include RelationHelpers
  include Ressourceable
  include Skillable

  algoliasearch disable_indexing: !Rails.env.production?, if: :submitted_at? do
    use_serializer Api::ProposalSerializer
  end

  resourcify

  ## FIELDS ##
  belongs_to :peer_review
  belongs_to :project

  has_many :answers, dependent: :destroy

  ## VALIDATIONS ##
  validates :peer_review_id, uniqueness: { scope: :project_id }

  ## METHODS ##
  def frontend_link
    link = short_title.nil? ? id : short_title

    "/proposal/#{link}"
  end

  def submitted?
    submitted_at.present?
  end
end
