# frozen_string_literal: true

class Project < ApplicationRecord
  include AffiliatableChild
  include AlgoliaSearch
  include Bannerable
  include Coordinates
  include Feedable
  include Geocodable
  include Interestable
  include Linkable
  include Mediumable
  include Membership
  include NotificationsHelpers
  include RecsysHelpers
  include RelationHelpers
  include Skillable
  include Utils

  belongs_to :creator, foreign_key: 'creator_id', class_name: 'User'
  belongs_to :space, optional: true
  belongs_to :workgroup, optional: true

  enum status: %i[active archived draft completed on_hold terminated]
  enum maturity: %i[problem_statement solution_proposal prototype proof_of_concept implementation demonstrated_impact]

  has_many :challenges_projects
  has_many :challenges,
           through: :challenges_projects,
           after_add: %i[update_activity reindex],
           after_remove: %i[update_activity reindex]
  has_many :customfields, as: :resource
  has_many :externalhooks, as: :hookable, after_add: %i[update_activity], after_remove: %i[update_activity]
  has_many :needs_projects
  has_many :needs, through: :needs_projects, after_add: %i[update_activity], after_remove: %i[update_activity]
  has_many :proposals, dependent: nil

  has_many_attached :documents

  validates :short_title, uniqueness: true
  validates :title, presence: true, uniqueness: true

  after_validation :geocode

  before_create :create_coordinates
  before_create :sanitize_description
  before_update :sanitize_description

  geocoded_by :make_address
  has_merit
  notification_object
  resourcify
  valid_affiliations :space

  algoliasearch disable_indexing: !Rails.env.production?, unless: :draft? do
    use_serializer Api::ProjectSerializer
    geoloc :latitude, :longitude
  end

  def needs_count
    needs.count
  end

  def update_activity(_obj)
    self.updated_at = Time.now
  end

  def frontend_link
    "/project/#{id}"
  end

  def notif_pending_join_request(requestor)
    Notification.create(
      target: requestor,
      category: :membership,
      type: 'pending_join_request',
      object: self
    )
  end

  def notif_pending_join_request_approved(requestor)
    Notification.create(
      target: requestor,
      category: :membership,
      type: 'pending_join_request_approved',
      object: self
    )
  end

  def clean_destroy
    needs.each(&:destroy)
    documents.each(&:purge)
    externalhooks.each(&:destroy)
    destroy
  end

  private

  def default_banner_url
    ActionController::Base.helpers.image_url('default-project.jpg')
  end
end
