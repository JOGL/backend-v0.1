# frozen_string_literal: true

class Comment < ApplicationRecord
  include RecsysHelpers
  include RelationHelpers
  include Utils

  belongs_to :post
  belongs_to :user

  before_create :sanitize_content
  after_create :notif_new_comment
  before_update :sanitize_content

  validates :content, presence: true

  notification_object
  resourcify

  def frontend_link
    if post
      "/post/#{post.id}"
    else
      # quick fix for orphaned comment breaking notifications
      # longer-term fix: don't orphan comments
      '/404'
    end
  end

  def notif_new_comment
    Notification.for_group(
      :post_participants_except_commenter,
      args: [post, self],
      attrs: {
        category: :comment,
        type: 'new_comment',
        object: self,
        metadata: {
          commentor: user,
          post: post
        }
      }
    )
  end
end
