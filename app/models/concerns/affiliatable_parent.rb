# frozen_string_literal: true

module AffiliatableParent
  extend ActiveSupport::Concern

  included do
    has_many :affiliations, as: :parent

    def self.valid_affiliates(*valid_affiliate_types)
      @@valid_affiliate_types = valid_affiliate_types
    end
  end

  def add_affiliate(affiliate)
    # add validation that we're not affiliating to self
    affiliation = Affiliation.new(parent: self, affiliate: affiliate, status: "accepted")
    if @@valid_affiliate_types.include?(affiliate.class.to_s.downcase.to_sym)
      affiliation.save
      affiliation
    else
      affiliation.errors.add :affiliate, ":#{affiliate.class.to_s.downcase} type invalid, must be one of #{@@valid_affiliate_types.inspect}"
      affiliation
    end
  end

  def remove_affiliate(affiliate)
    ::Affiliation.destroy_by(parent: self, affiliate: affiliate)
  end
end
