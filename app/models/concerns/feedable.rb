# frozen_string_literal: true

module Feedable
  extend ActiveSupport::Concern

  included do
    has_one :feed, as: :feedable, dependent: :destroy

    after_create :create_feed
    after_create :set_allow_posting_to_all, if: proc { %w[space].include? self.class.name.downcase }
  end

  def posts_count
    feed.posts.size
  end

  private

  def set_allow_posting_to_all
    feed.update(allow_posting_to_all: false)
  end
end
