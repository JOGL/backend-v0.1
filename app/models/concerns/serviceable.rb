# frozen_string_literal: true

# Add multiple Service(s) to an object.
#
# @example Add a service to an object
#   service = Service.new(creator: User.first, name: :eventbrite, token: 'FOO')
#   Space.first.services << service
module Serviceable
  extend ActiveSupport::Concern

  included do
    has_many :services, as: :serviceable, dependent: :destroy
    has_many :activities, through: :services
  end

  # Can a service be created?
  # @example Check whether or not a User(id: 1) can attach a Service to a Space(id: 1)
  #   Serviceable.serviceable?('space', 1, 1)
  #
  # @param [String] serviceable_type the class of object to be serviced
  # @param [Integer] serviceable_id the id of the object to be serviced
  # @param [Integer] user_id the id of the user creating the service
  # @return [Boolean] whether or not the entity's class includes serviceable
  def self.serviceable?(serviceable_type, serviceable_id, user_id)
    return false if [serviceable_type, serviceable_id, user_id].any?(&:nil?)

    klass = serviceable_type.camelize.constantize

    klass.include?(Serviceable) and klass.exists?(id: serviceable_id) and User.exists?(id: user_id)
  end
end
