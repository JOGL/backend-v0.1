# frozen_string_literal: true

module AffiliatableChild
  extend ActiveSupport::Concern

  included do
    has_many :affiliations, as: :affiliate

    def self.valid_affiliations(*valid_affiliation_types)
      @@valid_affiliation_types = valid_affiliation_types
    end
  end

  def add_affiliation_to(parent)
    if @@valid_affiliation_types.include?(parent.class.name.underscore.to_sym)
      ::Affiliation.create(parent: parent, affiliate: self)
    else
      raise "Invalid affiliation type #{parent.class.name.underscore.to_sym}, must be one of #{@@valid_affiliation_types.inspect}"
    end
  end

  def remove_affiliation_to(parent)
    ::Affiliation.destroy_by(parent: parent, affiliate: self)
  end
end
