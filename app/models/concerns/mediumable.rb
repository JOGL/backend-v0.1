# frozen_string_literal: true

module Mediumable
  extend ActiveSupport::Concern

  included do
    has_many :media, as: :mediumable
  end
end
