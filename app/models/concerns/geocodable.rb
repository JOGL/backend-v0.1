module Geocodable
  extend ActiveSupport::Concern

  def make_address
    [address, city, country].compact.join(', ')
  end
end
