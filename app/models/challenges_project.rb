# frozen_string_literal: true

class ChallengesProject < ApplicationRecord
  belongs_to :challenge
  belongs_to :project

  enum project_status: { pending: 0, accepted: 1 }

  after_update :send_notification_after_change

  scope :accepted_projects, -> { where(project_status: 'accepted') }
  scope :accepted_count, -> { accepted_projects.count }

  private

  def send_notification_after_change
    notif_pending_join_request_approved if saved_change_to_attribute?(:project_status) && accepted?
  end

  def notif_pending_join_request_approved
    Notification.for_group(
      :admins,
      args: [project],
      attrs: {
        category: :administration,
        type: 'pending_join_request_approved',
        object: project,
        metadata: {
          challenge: challenge
        }
      }
    )
  end
end
