# frozen_string_literal: true

class UsersBoard < ApplicationRecord
  belongs_to :board
  belongs_to :user
end
