# frozen_string_literal: true

class PeerReview < ApplicationRecord
  include AlgoliaSearch
  include Bannerable
  include Feedable
  include Interestable
  include Membership
  include RelationHelpers
  include Skillable

  resourcify

  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  enum visibility: { hidden: 0, open: 1 }

  has_many :proposals, dependent: :destroy

  has_one :faq, as: :faqable, dependent: :destroy

  ## VALIDATIONS ##
  validates :short_title, uniqueness: true

  algoliasearch disable_indexing: !Rails.env.production?, unless: :hidden? do
    use_serializer Api::PeerReviewSerializer
  end

  ## CALLBACKS ##
  after_create do |peer_review|
    peer_review.create_faq

    peer_review.options = { restrict_to_parent_members: false }
  end

  ## METHODS ##
  def frontend_link
    link = short_title.nil? ? id : short_title

    "/peer-review/#{link}"
  end
end
