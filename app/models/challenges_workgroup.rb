# frozen_string_literal: true

class ChallengesWorkgroup < ApplicationRecord
  belongs_to :challenge
  belongs_to :workgroup
end
