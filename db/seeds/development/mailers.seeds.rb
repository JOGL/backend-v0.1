# frozen_string_literal: true

after 'development:users', 'development:spaces' do
  user = User.new(id: 2, email: 'foo@internet.com', first_name: 'Foo', last_name: 'Bar')

  DeviseMailer.confirmation_instructions(User.first, 'abcd').deliver_now

  DeviseMailer.reset_password_instructions(User.first, 'abcd').deliver_now

  InviteStrangerMailer.invite_stranger(User.first, Space.first, user.email).deliver_now

  InviteUserMailer.invite_user(User.first, Space.first, user).deliver_now

  PrivateMailer.send_private_email(User.first, user, nil, 'Foo Bar Baz').deliver_now

  RecsysMailer.recsys_recommendation(User.first, '', Need.all, Need.all.collect { |_| 1 }).deliver_now

  UserJoinedMailer.user_joined(User.first, Space.first, user).deliver_now
end
