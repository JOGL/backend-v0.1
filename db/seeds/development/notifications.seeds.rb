# frozen_string_literal: true

after 'development:users', 'development:spaces', 'development:challenges', 'development:projects' do
  # Affiliation Accepted
  NotificationMailer.notify(
    Notification.create(target: User.first, type: 'affiliation_accepted', object: Project.first, metadata: { parent: Space.first }, category: :administration)
  ).deliver_now
  # Affiliation Destroyed
  NotificationMailer.notify(
    Notification.create(target: User.first, type: 'affiliation_destroyed', object: Project.first, metadata: { parent: Space.first }, category: :administration)
  ).deliver_now
  # Affiliation Pending
  NotificationMailer.notify(
    Notification.create(target: User.first, type: 'affiliation_pending', object: Space.first, metadata: { affiliate: Project.first }, category: :administration)
  ).deliver_now
  # Affiliation Rejected
  NotificationMailer.notify(
    Notification.create(target: User.first, type: 'affiliation_rejected', object: Project.first, metadata: { parent: Space.first }, category: :administration)
  ).deliver_now

  # End Challenge

  # End Program

  # New Post

  # New Post Admin Notify

  # New Post on Profile Feed

  # New Clap

  # New Comment

  # New Follower

  # New Member

  # New Mention

  # New Need

  # New Program

  # New User Follower

  # Pending Join Request

  # Pending Join Request Approved

  # Pending Member

  # Pending Project

  # Rejected Project
end
