# frozen_string_literal: true

after 'development:users', 'development:projects', 'development:challenges' do
  Challenge.first.projects << Project.third
end
