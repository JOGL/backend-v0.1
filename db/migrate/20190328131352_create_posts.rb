# frozen_string_literal: true

class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.belongs_to :feed
      t.belongs_to :user
      t.string :content
      t.string :media
      t.timestamps
    end

    create_table :feeds_posts, id: false do |t|
      t.belongs_to :feed, index: true
      t.belongs_to :post, index: true
    end
  end
end
