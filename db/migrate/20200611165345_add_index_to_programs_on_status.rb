class AddIndexToProgramsOnStatus < ActiveRecord::Migration[5.2]
  def change
    add_index :programs, :status
  end
end
