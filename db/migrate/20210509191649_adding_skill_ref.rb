class AddingSkillRef < ActiveRecord::Migration[6.1]
  def change
    create_table :recsys_skills_referential do |t|
      # Add the acronym table for NLP
      t.string :skill_name
      t.string :clean_skill_name
      t.string :enriched_skill_name
      t.string :stemmed_skill_name
      t.string :lg_skill
      t.boolean :is_linkedin_skill
      t.integer :count
      t.timestamps
    end
  end
end
