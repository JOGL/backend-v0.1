class AddHomeHeaderAndInfoToSpaces < ActiveRecord::Migration[5.2]
  def change
    add_column :spaces, :home_header, :string, default: ''
    add_column :spaces, :home_info, :string, default: ''
  end
end
