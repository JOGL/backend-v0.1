class CreateAnswers < ActiveRecord::Migration[6.1]
  def change
    create_table :answers do |t|
      t.references :document
      t.references :proposal
      
      t.string :content

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
