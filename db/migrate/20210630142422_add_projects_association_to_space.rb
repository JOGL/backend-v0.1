class AddProjectsAssociationToSpace < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :space_id, :integer, null: true
  end
end
