# frozen_string_literal: true

class CreateLicenses < ActiveRecord::Migration[5.2]
  def change
    create_table :licenses do |t|
      t.string :title
      t.string :short_title
      t.string :version
      t.string :attr
      t.string :logo_url
      t.string :url
      t.string :url_legal
      t.bigint :licenseable_id
      t.string :licenseable_type

      t.timestamps
    end
  end
end
