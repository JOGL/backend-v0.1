# frozen_string_literal: true

class ProgramAddNickname < ActiveRecord::Migration[5.2]
  def change
    add_column :programs, :short_title, :string
  end
end
