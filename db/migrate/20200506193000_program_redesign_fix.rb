# frozen_string_literal: true

class ProgramRedesignFix < ActiveRecord::Migration[5.2]
  def change
    # rename_column :users, :status, :active_status
    add_column :users, :job_title, :string
  end
end
