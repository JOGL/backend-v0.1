class AddIsReviewedToProjects < ActiveRecord::Migration[6.1]
  def change
    add_column :projects, :is_reviewed, :boolean, default: false
  end
end
