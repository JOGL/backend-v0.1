# frozen_string_literal: true

class AddUserLogourl < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :logo_url, :string
  end
end
