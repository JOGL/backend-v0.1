class AddLogoToActivities < ActiveRecord::Migration[6.1]
  def change
    add_column :activities, :logo, :string
  end
end
