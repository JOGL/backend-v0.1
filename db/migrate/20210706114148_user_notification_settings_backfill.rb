class UserNotificationSettingsBackfill < ActiveRecord::Migration[6.1]
  def data
    User.all.each do |user|
      user.settings.categories!.recsys!.enabled = true
      user.settings.categories!.recsys!.delivery_methods!.email!.enabled = true
      user.save
    end
  end
end
