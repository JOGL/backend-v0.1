class AddAllowPostingToAllToFeeds < ActiveRecord::Migration[6.1]
  def change
    add_column :feeds, :allow_posting_to_all, :boolean, default: true
  end
end
