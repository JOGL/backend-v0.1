class CreateDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :documents do |t|
      t.string :title
      t.string :content
      t.bigint :documentable_id
      t.string :documentable_type
      t.timestamps
    end

    create_table :authorships do |t|
      t.bigint :user_id
      t.bigint :authorable_id
      t.string :authorable_type
      t.timestamps
    end
  end
end
