class CreatePeerReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :peer_reviews do |t|
      t.references :resource, polymorphic: true

      t.datetime :deadline
      t.string :description
      t.float :maximum_disbursement
      t.jsonb :options, null: false, default: {}
      t.string :rules
      t.float :score_threshold
      t.string :short_title
      t.datetime :start
      t.datetime :stop
      t.datetime :submitted_at
      t.string :summary
      t.string :title, null: false
      t.float :total_funds, null: false, default: 0
      t.integer :visibility, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :peer_reviews, :short_title, unique: true

    create_table :interests_peer_reviews, id: false do |t|
      t.integer :interest_id
      t.integer :peer_review_id
    end

    create_table :peer_reviews_skills, id: false do |t|
      t.integer :peer_review_id
      t.integer :skill_id
    end
  end
end
