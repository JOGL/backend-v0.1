class CreateServiceFlowData < ActiveRecord::Migration[6.1]
  def change
    create_table :service_flow_data do |t|
      t.integer :service_id, null: false
      t.integer :flow, null: false
      t.jsonb :data, null: false, default: '{}'

      t.datetime :created_at, null: false, default: ->{ 'now()' }
      t.datetime :updated_at, null: false, default: ->{ 'now()' }
    end

    add_foreign_key :service_flow_data, :services
    
    add_index :service_flow_data, :data, using: :gin
    add_index :service_flow_data, [:service_id, :flow], unique: true, name: 'unique_service_flow_data_index'
  end
end
